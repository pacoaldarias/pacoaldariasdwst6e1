
--
-- Base de datos: `discografia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `codigo` int(7) NOT NULL,
  `titulo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `discografica` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `formato` enum('cassette','vinilo','cd','dvd','mp3') COLLATE latin1_spanish_ci NOT NULL,
  `fechaLanzamiento` date DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL,
  `precio` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `album`
--

INSERT INTO `album` (`codigo`, `titulo`, `discografica`, `formato`, `fechaLanzamiento`, `fechaCompra`, `precio`) VALUES
(1, 'Enemigos de lo ajeno', 'sony', 'cd', NULL, NULL, NULL),
(2, 'Como la cabeza al sombrero', 'Sony', 'cd', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cancion`
--

CREATE TABLE IF NOT EXISTS `cancion` (
  `titulo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `album` int(7) NOT NULL,
  `posicion` int(2) DEFAULT NULL,
  `duracion` time DEFAULT NULL,
  `version` enum('S','N') COLLATE latin1_spanish_ci DEFAULT NULL,
  `genero` enum('acustica','banda sonora','blues','electronica','folk','jazz','new age','pop','rock') COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `cancion`
--

INSERT INTO `cancion` (`titulo`, `album`, `posicion`, `duracion`, `version`, `genero`) VALUES
('Insurrección', 1, NULL, NULL, 'N', 'pop'),
('Las palabras son cansancio', 1, NULL, NULL, 'N', 'pop'),
('Llanto de pasión', 2, NULL, NULL, 'N', 'pop'),
('Sara', 2, NULL, NULL, 'N', 'pop'),
('Ya no danzo al son de los tambores', 2, NULL, NULL, 'N', 'pop');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `album`
--
ALTER TABLE `album`
 ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `cancion`
--
ALTER TABLE `cancion`
 ADD PRIMARY KEY (`titulo`,`album`), ADD KEY `album` (`album`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cancion`
--
ALTER TABLE `cancion`
ADD CONSTRAINT `cancion_ibfk_1` FOREIGN KEY (`album`) REFERENCES `album` (`codigo`) ON DELETE CASCADE;

