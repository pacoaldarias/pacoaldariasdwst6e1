<HTML LANG="es">

  <HEAD>
    <TITLE>Encuesta. Resultados de la votación</TITLE>
    <LINK REL="stylesheet" TYPE="text/css" HREF="estilo.css">
  </HEAD>

  <BODY>

    <H1>Encuesta. Resultados de la votació</H1>

    <?PHP
    // Conectar con la base de datos
    // Conectar con la base de datos

    DEFINE("DB_HOST", "localhost");
    DEFINE("DB_USER", "alumno");
    DEFINE("DB_PASSWORD", "alumno");
    DEFINE("DB_NAME", "discografia");

    //conectamos con la BD
    try {
      $con = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
    } catch (PDOException $e) {
      print "¡Error!: " . $e->getMessage() . "<br/>";
      die();
    }

    // Obtener datos actuales de la votaci�n
    $instruccion = "select votos1,votos2 from votos";

    $res = $con->query($instruccion);
    $fila = $res->fetch();



    $votos1 = $fila[0];
    $votos2 = $fila[1];
    $totalVotos = $votos1 + $votos2;

    // Mostrar resultados
    print ("<TABLE>\n");

    print ("<TR>\n");
    print ("<TH>Respuesta</TH>\n");
    print ("<TH>Votos</TH>\n");
    print ("<TH>Porcentaje</TH>\n");
    print ("<TH>Representacion grafica</TH>\n");
    print ("</TR>\n");

    $porcentaje = round(($votos1 / $totalVotos) * 100, 2);
    print ("<TR>\n");
    print ("<TD CLASS='izquierda'>SI</TD>\n");
    print ("<TD CLASS='derecha'>$votos1</TD>\n");
    print ("<TD CLASS='derecha'>$porcentaje%</TD>\n");
    print ("<TD CLASS='izquierda'><IMG SRC='img/puntoamarillo.gif' HEIGHT='10' WIDTH='" .
      $porcentaje * 4 . "'></TD>\n");
    print ("</TR>\n");

    $porcentaje = round(($votos2 / $totalVotos) * 100, 2);
    print ("<TR>\n");
    print ("<TD CLASS='izquierda'>No</TD>\n");
    print ("<TD CLASS='derecha'>$votos2</TD>\n");
    print ("<TD CLASS='derecha'>$porcentaje%</TD>\n");
    print ("<TD CLASS='izquierda'><IMG SRC='img/puntoamarillo.gif' HEIGHT='10' WIDTH='" .
      $porcentaje * 4 . "'></TD>\n");
    print ("</TR>\n");

    print ("</TABLE>\n");

    print ("<P>Numero total de votos emitidos: $totalVotos </P>\n");

    print ("<P><A HREF='ejercicio2.php'>Pagina de votacion</A></P>\n");

    // Desconectar
    $con = null;
    ?>

  </BODY>
</HTML>
