﻿-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-11-2014 a las 12:52:10
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `encuestas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE IF NOT EXISTS `encuesta` (
`id` int(10) NOT NULL,
  `textoPregunta` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`id`, `textoPregunta`) VALUES
(1, '¿Crees que aprobarás el examen?'),
(2, '¿Quien ganará el mundial de fórmula uno?');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responde`
--

CREATE TABLE IF NOT EXISTS `responde` (
  `usuario` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `idEncuesta` int(10) NOT NULL,
  `cuando` datetime NOT NULL,
  `desde` varchar(15) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta`
--

CREATE TABLE IF NOT EXISTS `respuesta` (
`id` int(10) NOT NULL,
  `idEncuesta` int(10) DEFAULT NULL,
  `textoRespuesta` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `numeroRespuestas` int(10) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `respuesta`
--

INSERT INTO `respuesta` (`id`, `idEncuesta`, `textoRespuesta`, `numeroRespuestas`) VALUES
(1, 1, 'Por supuesto, mínimo un 10', 27),
(2, 1, 'Con un 5 ya me conformaba yo', 3),
(3, 1, 'Puede que sí y puede que no', 4),
(4, 1, 'Ni de coña', 3),
(5, 2, 'Kimi Raikkonen', 4),
(6, 2, 'Fernando Alonso', 3),
(7, 2, 'Felipe Massa', 1),
(8, 2, 'Lewis Hamilton', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `login` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `password` binary(40) NOT NULL,
  `tipoUsuario` enum('admin','user') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `responde`
--
ALTER TABLE `responde`
 ADD PRIMARY KEY (`usuario`,`idEncuesta`), ADD KEY `idEncuesta` (`idEncuesta`);

--
-- Indices de la tabla `respuesta`
--
ALTER TABLE `respuesta`
 ADD PRIMARY KEY (`id`), ADD KEY `idencuesta` (`idEncuesta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`login`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `encuesta`
--
ALTER TABLE `encuesta`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `respuesta`
--
ALTER TABLE `respuesta`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `responde`
--
ALTER TABLE `responde`
ADD CONSTRAINT `responde_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`login`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `responde_ibfk_2` FOREIGN KEY (`idEncuesta`) REFERENCES `encuesta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuesta`
--
ALTER TABLE `respuesta`
ADD CONSTRAINT `respuesta_ibfk_1` FOREIGN KEY (`idencuesta`) REFERENCES `encuesta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
