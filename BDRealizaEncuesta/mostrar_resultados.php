<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Resultados</title>
  </head>
  <body>
      <?php
      //Hemos dejado marcado por defecto un valor en el radiobutton. Vamos a capturar lo que contiene (si lo ha cambiado el usuario)
      $id_respuesta = $_REQUEST['respuesta'];

      //Conectamos con la BD
      include("conectar_db.php");

      //Creamos la consulta sql para obtener la informaci�n
      $query = "SELECT * FROM respuesta WHERE id=$id_respuesta";

      $res = $con->query($instruccion);
      $fila = $res->fetch();



      //Extraemos los datos de la consulta
      $idEncuesta_respuesta = $fila[0];
      $textoRespuesta_respuesta = $fila[1];
      $numeroRespuestas_respuesta = $fila[2];

      //Aumentamos el contador del n�mero de respuestas y lo modificamos en la BD (campo numeroRespuestas, tabla respuesta)
      $numeroRespuestas_respuesta++;
      $query = "UPDATE respuesta SET numeroRespuestas=$numeroRespuestas_respuesta where id=$id_respuesta";
      mysql_query($query, $con);


      //Vamos a mostrar los datos en pantalla
      //1. Seleccionamos las filas de nuestra encuesta (id)
      $query = "SELECT * FROM respuesta WHERE idEncuesta=$idEncuesta_respuesta";
      $result_respuesta = mysql_query($query, $con);
      //2.Hacemos la consulta para la encuesta seleccionada
      $query = "SELECT textoPregunta FROM encuesta where id=$idEncuesta_respuesta";
      $result_encuesta = mysql_query($query, $con);
      $result_encuesta = mysql_result($result_encuesta, 0, "textoPregunta");
      //3.Calculamos cu�ntas respuestas han habido en la encuestacon una query
      $query = "SELECT SUM(numeroRespuestas) FROM respuesta where idEncuesta=$idEncuesta_respuesta";
      $result_suma = mysql_query($query, $con);
      $result_suma = mysql_result($result_suma, 0);
      //4. Mostramos los resultados en una tabla
      echo "<h1>RESULTADOS ENCUESTA #" . $idEncuesta_respuesta . "</h1>";
      echo "<table border='1' width='80%'>";
      echo "<tr  align='left'>";
      echo "<td>" . $result_encuesta . "</td>";
      echo "<td>Total votaciones: " . $result_suma . "</td>";
      echo "</tr>";

      while ($fila = mysql_fetch_array($result_respuesta)) {
        $porcentaje = round((100 * $fila['numeroRespuestas']) / $result_suma, 0);
        echo "<tr>";
        echo "<td>" . $fila['textoRespuesta'] . "</td>";
        echo "<td>" . $porcentaje . "% (" . $fila['numeroRespuestas'] . " votos)</td>";
      }
      echo "</tr>";
      echo "<table>";

      //Cerramos la conexi�n
      mysql_close($con);
      ?>

    <!-- Back to Home  -->
    <br>
    <form action='selecciona_encuesta.php'>
      <input type='submit' value='Volver a la seleccion de encuesta'/>
    </form>
  </body>
</html>

