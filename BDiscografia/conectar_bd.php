<?php

//guardamos los valores de la conexion
DEFINE("DB_HOST", "localhost");
DEFINE("DB_USER", "alumno");
DEFINE("DB_PASSWORD", "alumno");
DEFINE("DB_NAME", "discografia");

//conectamos con la BD
try {
  $con = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
} catch (PDOException $e) {
  print "¡Error!: " . $e->getMessage() . "<br/>";
  die();
}