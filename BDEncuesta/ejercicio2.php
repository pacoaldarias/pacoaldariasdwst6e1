<HTML LANG="es">

  <HEAD>
    <TITLE>Encuesta</TITLE>
    <LINK REL="stylesheet" TYPE="text/css" HREF="estilo.css">
  </HEAD>

  <BODY>

    <?PHP
    $enviar = $_REQUEST['enviar'];
    if (isset($enviar)) {
      print ("<H1>Encuesta. Voto registrado</H1>\n");

      // Conectar con la base de datos

      DEFINE("DB_HOST", "localhost");
      DEFINE("DB_USER", "alumno");
      DEFINE("DB_PASSWORD", "alumno");
      DEFINE("DB_NAME", "discografia");

      //conectamos con la BD
      try {
        $con = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
      } catch (PDOException $e) {
        print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
      }

      // Obtener votos actuales
      $instruccion = "select votos1, votos2 from votos";

      $res = $con->query($instruccion);
      $fila = $res->fetch();

      if (!$res) {
        print "<p>Error en la consulta.</p>\n";
      } else {
        print "<p>Consulta Select ejecutada.</p>\n";
      }


      // Actualizar votos
      $votos1 = $fila["0"];
      $votos2 = $fila["1"];
      $voto = $_REQUEST['voto'];
      if ($voto == "si")
        $votos1 = $votos1 + 1;
      else if ($voto == "no")
        $votos2 = $votos2 + 1;


      $instruccion = "update votos set votos1=$votos1, votos2=$votos2";

      if ($con->query($instruccion)) {
        print "<p>Actualizado correctamente.</p>\n";
      } else {
        print "<p>Actualizado correctamente.</p>\n";
      }

      // Desconectar
      // Mostrar mensaje de agradecimiento
      print ("<P>Su voto ha sido registrado. Gracias por participar</P>\n");
      print ("<A HREF='ejercicio2-resultados.php'>Ver resultados</A>\n");
    } else {
      ?>

      <H1>Encuesta</H1>

      <P>Cree ud. que el precio de la vivienda seguirá subiendo al ritmo actual?</P>

      <FORM ACTION="ejercicio2.php" METHOD="POST">
        <INPUT TYPE="RADIO" NAME="voto" VALUE="si" CHECKED>SI<BR>
        <INPUT TYPE="RADIO" NAME="voto" VALUE="no">NO<BR><BR>
        <INPUT TYPE="SUBMIT" NAME="enviar" VALUE="votar">
      </FORM>

      <A HREF="ejercicio2-resultados.php">Ver resultados</A>

      <?PHP
    }
    ?>

  </BODY>
</HTML>
